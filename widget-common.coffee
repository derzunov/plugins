###
* Класс с набором стандартных методов,
* от которого в дальнейшем будут наследоваться другие классы
###

`'use strict'`

((window, $) ->

  class WidgetCommon

    defaults:
      debug: false
      classes:
        hidden: 'g-hidden'
        loader: 'g-loader'

    ###*********PUBLIC METHODS*********###

    ###*
	  * @param $node {jQuery} - Элемент на который вешается виджет
	  * @param options {Object} - Опции, переданные при вызове
	  * @param defaults {Object} - Опции по-умолчанию, определенные в конкретном виджете
	  * @constructor
    ###
    constructor: ($node, options, defaults) ->

      defaults = defaults or {}

      # Формируем опции виджета
      @options = $.extend true,
        {}
        @defaults
        defaults
        options

      if !$node or !$node.size()
        console.error 'Incorrect $node in ' + @name + '.constructor'
        return
      else
        @$node = $node
        @node = $node.get 0

    ###*********PRIVATE METHODS*********###

    ###
    * Инициализация элементов
    ###
    _initElements: () ->

    ###
    * Навешивание событий
    ###
    _bindEvents: () ->

    ###
    * helper
    * Скрывает элемент методом добавления класса
    * @param {jQuery} $el - элемент, который нужно скрыть
    * @return {*}
    ###
    _hide: ($el) ->
      if @options.debug
        if not $el
          console.error 'param $el must be defined in ' + @name + '._hide'
        else if not $el instanceof jQuery
          console.error 'param $el must be jQuery object in ' + @name + '._hide'

      $el.addClass @options.classes.hidden

    ###
    * helper
    * Показывает элемент
    * @param {jQuery} $el - элемент, который нужно показать
    * @return {*}
    ###
    _show: ($el) ->
      if @options.debug
        if not $el
          console.error 'param $el must be defined in ' + @name + '._show'
        else if not $el instanceof jQuery
          console.error 'param $el must be jQuery object in ' + @name + '._show'

      if $el.hasClass @options.classes.hidden
        $el.removeClass @options.classes.hidden
      else
        $el.show()

    ###
    * helper
    * Показывает/скрывает элемент
    * @param {jQuery} $el - элемент, который нужно показать/скрыть
    * @return {*}
    ###
    _toggleShow: ($el) ->
      if @options.debug
        if not $el
          console.error 'param $el must be defined in ' + @name + '._toggleShow'
        else if not $el instanceof jQuery
          console.error 'param $el must be jQuery object in ' + @name + '._toggleShow'

      if $el.hasClass @options.classes.hidden
        $el.removeClass @options.classes.hidden

        if not $el.is(':visible')
          $el.show()
      else if not $el.hasClass @options.classes.hidden
        $el.addClass @options.classes.hidden

        if $el.is(':visible')
          $el.hide()

    ###
    * helper
    * Ищет элемент по классу в @$node
    * @param {String} classKey ключ класса для выбора из опций
    * @param {jQuery} $el если задан, то искать элемент в нем
    * @return {jQuery}
    ###
    _findByClass: (classKey, $el) ->
      if @options.debug
        if not classKey
          console.error 'param classKey must be defined in ' + @name + '._findByClass'
        else if not typeof classKey is 'string'
          console.error 'param $el must be string in ' + @name + '._findByClass'

      if $el
        return $el.find '.' + @options.classes[classKey]
      else
        return @$node.find '.' + @options.classes[classKey]

  window['WidgetCommon'] = WidgetCommon

) window, jQuery