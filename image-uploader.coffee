###
# Класс загрузчика изображений
# @class
###

((window, $) ->
  class ImageUploader

    ######### PUBLIC METHODS #########

    ###
    # @params {jQuery} $node элемент jQuery
    # @params {string} [options.url = '/upload/'] принимающий скрипт
    # @params {boolean} [options.customSize = false] принудительно выставлять размеры, переданные в опциях
    # @params {number} [options.width = 200] ширина
    # @params {number} [options.height = 200] высота
    # @params {number} [options.loadingOpacity = .7] уровень прозрачности слоя с загрузкой
    # @params {number} [options.loadingSpeed = 200] скорость анимации отображения загрузки
    # @params {string} [options.background] путь к фоновому изображению
    # @params {string} [options.classes.main = 'b-image-uploader'] класс основного контейнера
    # @params {string} [options.classes.iframe = 'image-uploader-transport'] класс ifram'a
    # @params {string} [options.classes.jsonResponseWrapper = 'image-uploader-result'] класс инпута, в котором будет содержаться json с ответом от сервера
    # @param {string} [options.classes.loading = 'image-uploader-loading'] класс элемента, обозначающего загрузку изображения
    # @param {string} [options.classes.selected = 'image-uploader-selected'] класс, добавляемый элементу после выбора изображения
    # @constructor
    ###
    constructor: ($node, options) ->
      @options = $.extend true,
        url: '/upload/'
        customSize: false
        width: 200,
        height: 200
        loadingOpacity: .7
        loadingSpeed: 0
        classes:
          main: 'b-image-uploader'
          iframe: 'image-uploader-transport'
          jsonResponseWrapper: 'image-uploader-result'
          loading: 'image-uploader-loading'
          imageContainer: 'image-uploader-image-container'
          selected: 'image-uploader-selected'
        options

      if !$node or !$node.size()
        console.error 'incorrect $node in ImageUploader.constructor'
        return

      @$node = $node
      @$node.addClass @options.classes.main

      @_createIframe()
      @_createLoadingLayout()
      @_createImageContainer()
      if @options.background
        @setBackground @options.background
      @_bindEvents()

      if @options.customSize
        @setSize
          width: @options.width
          height: @options.height

    ###
    # Установка размеров
    # @param {number} size.width ширина
    # @param {number} size.height высота
    ###
    setSize: (size) ->
      if !size
        @$iframe.css
          width: @$node.width()
          height: @$node.height()
      else
        if !size.lineHeight
          size.lineHeight = size.height + 'px'
        @$node.add(@$imageContainer).css size

    ###
    # Установка фонового изображения
    # @param {string} background путь к фоновому изображению
    ###
    setBackground: (background) ->
      if !background
        return
      @$node.css
        'background-image': 'url("' + background + '")'


    ######### PRIVATE METHODS #########

    ###
    # Создание ifram'a и его содержимого
    ###
    _createIframe: ->
      @$iframe = $('<iframe />')
        .addClass(@options.classes.iframe)
        .appendTo(@$node)

      @$form = $('<form />')
        .attr
          enctype: 'multipart/form-data'
          method: 'post'
          action: @options.url
      @$input = $('<input type="file" name="image" >');

      @$input.appendTo @$form
      @$form.appendTo @$iframe.contents().find('body')

    ###
    # Обработка ответа от сервера
    ###
    _processResponse: ->
      jsonString = @$form.find('.' + @options.classes.jsonResponseWrapper).val()
      json = $.parseJSON jsonString
      
      if !json
        console.error 'Upload failed'
        return

      if json.error
        @options.onError && @options.onError.call @, json.error
        console.error 'ImageUploader: ' + json.error
        return

      if json.success
        @options.onSuccess && @options.onSuccess.call @, json.fileName

      @_setUploadFile json.fileName

    ###
    # Устанавливает загруженное изображение на фон
    # @param {string} fileName путь к файлу
    ###
    _setUploadFile: (fileName) ->
      self = @
      @_loadingOn()
      img = new Image()
      img.onload = () ->
        self._loadingOff () =>
          self.$imageContainer.html img
          self.$node
            .css('background-image': 'none')
            .addClass self.options.classes.selected

      img.src = '//' + location.hostname + fileName;

    ###
    # Создание слоя для обозначения загрузки изображения
    ###
    _createLoadingLayout: ->
      @$loadingLayout = $('<div>')
        .addClass(@options.classes.loading)
        .appendTo(@$node)

    ###
    # Создание контейнера для  изображения
    ###
    _createImageContainer: ->
      @$imageContainer = $('<div>')
        .addClass(@options.classes.imageContainer)
        .appendTo @$node

    ###
    # Включение загрузки
    # @param {function} callback функция, которая выполнится после завершения анимации
    ###
    _loadingOn: (callback) ->
      @$loadingLayout
        .show()
        .css
          opacity: 0
        .animate
          opacity: @options.loadingOpacity
        ,@options.loadingSpeed
        ,() =>
          callback && callback.call @

    ###
    # Выключение загрузки
    # @param {function} callback функция, которая выполнится после завершения анимации
    ###
    _loadingOff: (callback)->
      @$loadingLayout
        .animate
          opacity: 0
        ,@options.loadingSpeed
        ,() =>
          @$loadingLayout.hide()
          callback && callback.call @

    ###
    # Навешивание событий
    ###
    _bindEvents: ->
      @$node.on 'click', (event) => @_onClick event
      @$input.on 'change', (event) => @_onSelectFile event
      @$iframe.on 'load', (event) => @_onIframeLoad event

    ######### EVENTS HANDLERS #########

    ###
    # Обработчик события клика
    # @param {jQuery.Event} объект события
    ###
    _onClick: (event) ->
      @$input.trigger 'click'

    ###
    # Обработчик события выбора файла
    # @param {jQuery.Event} объект события
    ###
    _onSelectFile: (event) ->
      @$form.trigger 'submit'

    ###
    # Обработчик события загрузки ifram'a
    # @param {jQuery.Event} объект события
    ###
    _onIframeLoad: (event) ->
      @$form = @$iframe.contents().find('body').find('form');
      @$input = @$form.find('input[type="file"]');
      @$input.on 'change', (event) => @_onSelectFile event

      @_processResponse();

  createJQPlugin ImageUploader

)(window, jQuery)