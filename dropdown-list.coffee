###
* Класс выпадающего списка
* Необходим WidgetCommon
###

((window, $) ->

  widgetName = 'DropdownList'

  class DropdownList extends WidgetCommon

    defaults:
      classes:
        replaceable: 'dropdown-replaceable'
        select: 'dropdown-select'
        selectValue: 'dropdown-value'
        firstList: 'dropdown-list-first'
        firstItem: 'dropdown-item-first'
        secondList: 'dropdown-list-second'
        secondItem: 'dropdown-item-second'
        clickableFirst: 'clickable-first'
        hidden: 'g-hidden'
        scrolled: 'scrolled'
      dataNames:
        optionValue: 'option-value'
        dropdownValue: 'dropdown-value'
      scrollHandlerOn: true
      # Номер изначально раскрытого узла ( начиная с 0 )
      openedItem: 1

    ###*********PUBLIC METHODS*********###

    ###
    * @param $node {jQuery} - Элемент, на который вешается виджет
    * @param options {Object} - Переданные параметры
    ###

    constructor: ($node, options) ->
      self = @
      @$node = $node
      @name = widgetName
      super $node, options, @defaults

      @_initElements()
      @_bindEvents()

    _initElements: ()->
      self = @
      @$replaceable = @_findByClass 'replaceable'
      @$select = @_findByClass 'select'
      @$selectValue = @_findByClass 'selectValue'
      @$firstList = @_findByClass 'firstList'
      @$firstItems = @_findByClass 'firstItem'
      @$secondList = @_findByClass 'secondList'
      @$secondItems = @_findByClass 'secondItem'
      @$clickableFirst = @_findByClass 'clickableFirst'

      @_hide @$secondItems
      @_show @$firstItems.eq(1).find( '.' + self.options.classes.secondItem )

      @_hide @$firstList

    _bindEvents: ()->
      self = @

      @$firstItems.on 'click', () ->
        self._hide self.$secondItems
        self._show $(@).find('.' + self.options.classes.secondItem)

      @$secondItems.on 'click', () ->
        self.chooseOption $(@)

      @$clickableFirst.on 'click', () ->
        self.chooseOption $(@)

      @$select.on 'click', () ->
        self._toggleShow self.$firstList


      ###
      * Ловим клики вне элемента для его закрытия на потерю фокуса
      ###
      $(document.body).on 'click', () ->
        self._hide self.$firstList

      ###
      * Запрещаем всплытие клика выше нашего нода,
      * т.к обработчик клика вне висит на body и нам не надо, чтобы клик на нашу выпадайку всплыл до body
      ###
      self.$node.on 'click', (event) ->
        event.stopPropagation()

      ###
      * Обработка скролла для того, чтобы элемент "плыл" за прокруткой
      * Стиль элемента меняется в зависимости от того доскроллили мы до него или нет
      ###

      $(window).on 'scroll', () ->
        if self.options.scrollHandlerOn
          if $(@).scrollTop() >= self.$node.offset().top
            self.$node.addClass self.options.classes.scrolled
          else
            self.$node.removeClass self.options.classes.scrolled


    ###
    * Вставлям в дату нода выбранное нами значение при клике и триггерим событие изменения
    * @param {jQuery} element
    ###

    chooseOption: ( $element ) ->
      self = @
      if $element.data self.options.dataNames.optionValue
        self.$node.data self.options.dataNames.dropdownValue, $element.data self.options.dataNames.optionValue
        self.$node.trigger 'dropdown.valueChange'

      self.$node.trigger 'dropdown.change'

      self.$selectValue.html $element.html()
      setTimeout ()->
       self._hide self.$firstList
      , 400

      self.setActiveElement( $element )


    ###
    * Устанавливаем класс активности на элемент списка
    * @param {jQuery} element
    ###

    setActiveElement: ( $element ) ->
      self = @
      self.$secondItems.removeClass 'active'
      $element.addClass 'active'

  window[widgetName] = DropdownList

  createJQPlugin DropdownList

) window, jQuery